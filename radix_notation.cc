// radix_notation.cc

#include <string>
#include <iostream>

using namespace std;

// Implement this function.
string RadixNotation(unsigned int number, unsigned int radix);
int main() {
    while (true) {
    unsigned int number, radix;
    cin >> number >> radix;
     
    if (radix < 2 || radix > 36) break;
    
    cout << RadixNotation(number, radix) << endl;
    
  }
  return 0;
}
string RadixNotation(unsigned int number, unsigned int radix)
{
	string Notation="";
	int t,rest;
	int i=0;
	string tmp="";
 
	while(true)
	{
		t=number%radix;
		rest=number/radix;
		if(radix <=10)   
		{
			if(rest!=0)
			{
				tmp=static_cast<char>(t+48);
				Notation=tmp+Notation;
				number = rest;
			}
			if(rest==0)
			{
				Notation=static_cast<char>(number+48)+Notation;
				break;    
			}
		}
		if(radix>10)
		{
			if(rest!=0)
			{
				tmp=static_cast<char>(t+65-10);
				Notation=tmp+Notation;
				number=rest;
			}
			if(rest==0)
			{
				Notation=static_cast<char>(number+65-10)+Notation;
				break;
			}
		}
	}
	return Notation;
}
