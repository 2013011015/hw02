// simple_int_set_main.cc
#include<iostream>
#include<stdlib.h>
#include"simple_int_set.h"


using namespace std;

int main()
{
	while(true)
	{
		int tmp;
		int arr1[100],arr2[100];
		int *arr3;
		arr3 = new int [MAX_SIZE];
		int size1=0,size2=0;
		string str1="";
		string str2="";
		char op;
		SimpleIntSet a;
		SimpleIntSet b;
		SimpleIntSet result;
		cin>>str1;
		if(str1!="{")
			return 0;
		else
		{
			while(true)
			{
				cin>>str1;
				if(str1=="}")
					break;
				else
				{
					arr1[size1] = atoi(str1.c_str());
					size1++;
				}
			}
			a.Set(arr1,size1);
		}
		cin>>op;
		cin>>str2;
		if(str2!="{")
			return 0;
		else
		{
			while(true)
			{
				cin>>str2;
				if(str2=="}")
					break;
				else
				{
					arr2[size2]=atoi(str2.c_str());
					size2++;
				}
			}
			b.Set(arr2,size2);
		}
		if (op=='+')
			result=a.Union(b);
		else if (op=='-')
			result=a.Difference(b);
		else if (op=='*')
			result=a.Intersect(b);

		cout<<"{ ";
		for (int i=0;i<result.size();i++)
			cout<<result.values()[i]<<" ";
		cout<<"}"<<endl;
	}
}
