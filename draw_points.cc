// draw_points.cc
using namespace std;
#include <iostream>
#include <stdlib.h>

struct Point {
  unsigned int x, y;
};

// Implement this function.
void DrawPoints(const Point* points, int num_points);

int main()
{
	int num,i;
	while(true)
	{
		cin>>num;
		if(num>0)
		{
			Point *P=new Point[num];
			for (i=0;i<num;i++)
				cin>>P[i].x>>P[i].y;
			DrawPoints(P,num);
		}
		else
			return 0;
	}
}
void DrawPoints(const Point* points, int num_points)
{
	int largestX,largestY,i;
	char **map;
	largestX=largestY=0;
	for (i=0;i<num_points;i++)
	{
		if (points[i].x>largestX)
			largestX=points[i].x;
	}
	for (i=0;i<num_points;i++)
	{
		if (points[i].y>largestY)
			largestY=points[i].y;
	}
	map=(char **)malloc(sizeof(char)*(largestY+1));
	for(int i=0;i<largestY+1;i++)
		map[i]=(char *)malloc(sizeof(char)*(largestX+1));
	for(i=0;i<largestY+1;i++)
	{
		for(int j=0;j<largestX+1;j++)
			map[i][j]='.';
	}
	for(i=0;i<num_points;i++)
		map[points[i].y][points[i].x]='*';
	for(i=0;i<largestY+1;i++)
	{
		for(int j=0;j<largestX+1;j++)
			cout<<map[i][j];
		cout<<endl;	
	}
}

