// simple_int_set.cc
#include<iostream>
#include"simple_int_set.h"

SimpleIntSet SimpleIntSet::Intersect(const SimpleIntSet& int_set) const
{
	int i,j,arrl=0,min=values_[0],max=0;
	int* arr;
	SimpleIntSet c;
	arr = new int [MAX_SIZE];
	for (i=0;i<size_;i++)
	{
		if ( min> values_[i])
			min=values_[i];
	}
	for (i=0;i<size_;i++)
	{
		if (max< values_[i])
			max=values_[i];
	}
	for(i=0;i<int_set.size();i++)
	{
		if ( min> int_set.values_[i])
			min=int_set.values_[i];
	}
	for(i=0;i<int_set.size();i++)
	{
		if (max< int_set.values_[i])
			max=int_set.values_[i];
	}
	for (i=min;i<=max;i++)
	{
		int count=0;
		for (j=0;j<size_;j++)
		{
			if (i==values_[j])
			{
				count++;
				for(int k=0;k<int_set.size();k++)
				{
					if(i==int_set.values_[k])
						count++;
				}
			}
		}
		if (count==2)
		{
			arr[arrl]=i;
			arrl++;
		}
	}
	c.Set(arr,arrl);
	return c;
}	
SimpleIntSet SimpleIntSet::Union(const SimpleIntSet& int_set) const
{
	int i,j,arrl=0,min=values_[0],max=0;
	int* arr;
	SimpleIntSet c;
	arr = new int [MAX_SIZE];
	for (i=0;i<size_;i++)
	{
		if ( min> values_[i])
			min=values_[i];
	}
	for (i=0;i<size_;i++)
	{
		if (max< values_[i])
			max=values_[i];
	}
	for(i=0;i<int_set.size();i++)
	{
		if ( min> int_set.values_[i])
			min=int_set.values_[i];
	}
	for(i=0;i<int_set.size();i++)
	{
		if (max< int_set.values_[i])
			max=int_set.values_[i];
	}
	for (i=min;i<=max;i++)
	{
		int count=0;
		for (j=0;j<size_;j++)
		{
			if (i==values_[j])
				count++;
		}
		for(j=0;j<int_set.size();j++)
		{
			if (i==int_set.values_[j])
				count++;
		}
		if (count>0)
		{
			arr[arrl]=i;
			arrl++;
		}
	}
	c.Set(arr,arrl);
	return c;
}
SimpleIntSet SimpleIntSet::Difference(const SimpleIntSet& int_set) const
{
	int i,j,arrl=0,min=values_[0],max=0;
	int* arr;
	SimpleIntSet c;
	arr = new int [MAX_SIZE];
	for (i=0;i<size_;i++)
	{
		if ( min> values_[i])
			min=values_[i];
	}
	for (i=0;i<size_;i++)
	{
		if (max< values_[i])
			max=values_[i];
	}
	for(i=0;i<int_set.size();i++)
	{
		if ( min> int_set.values_[i])
			min=int_set.values_[i];
	}
	for(i=0;i<int_set.size();i++)
	{
		if (max< int_set.values_[i])
			max=int_set.values_[i];
	}
	for (i=min;i<=max;i++)
	{
		int count=0;
		for (j=0;j<size_;j++)
		{
			if (i==values_[j])
			{
				count++;
				for (int k=0;k<int_set.size();k++)
				{
					if(i==int_set.values_[k])
						count++;
				}
			}
		}
		if (count==1)
		{
			arr[arrl]=i;
			arrl++;
		}
	}
	c.Set(arr,arrl);
	return c;
}
void SimpleIntSet::Set(const int* values,int size)
{	
	int i;
	for (i=0;i< size;i++)
		values_[i]=values[i];
	size_=size;
}
